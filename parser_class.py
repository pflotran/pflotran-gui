# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 11:22:54 2020

@author: ande583
"""
import sys

class Parser(object):
    def __init__(self,lists):
        self.lists = lists
        self.final_opt = []
        self.axis = None
        
    def parse_first(self):
        if self.lists[0].startswith('TITLE'):
            options = self.lists[1]
            
            title = self.lists[0]
            variables = self.lists[1]
            zone = self.lists[2]
            
            a = zone.strip()
            b = a.rstrip('\n')
            c = b.split(',')

            if c[-1] == ' DATAPACKING=POINT':
                del self.lists[0:3]
            else:
                sys.exit('\n  Error: {} must be a point file format.\n'.format(__file__))
            
            dimensions = []
            
            for entry in c[3:6]:
                ent = entry.lstrip()
                num = ent.split('=')
                dimensions.append(num)
            
            for i in range(len(dimensions)):
                if int(dimensions[i][1]) != 0:
                    pass
                else:
                    sys.exit('\n  Error: {} must be 1-D.\n'.format(__file__))
                    
            for i in range(len(dimensions)):
                if int(dimensions[i][1]) != 1:
                    self.axis = dimensions[i][0]
                    
            for entry in options:
                opt = options.strip()
                opt2 = opt.split(',')
        
            for entry in opt2:
                string = entry.lstrip('"').rstrip('"')
                self.final_opt.append(string)
            
            if variables.startswith('VARIABLES'):
                s = self.final_opt[0].split('"')
                self.final_opt[0] = s[1]
                    
            if self.axis == 'I':
                del self.final_opt[1:3]
            elif self.axis == 'J':
                self.final_opt[0] = self.final_opt[1]
                del self.final_opt[1:3]
            elif self.axis == 'K':
                self.final_opt[0] = self.final_opt[2]  
                del self.final_opt[1:3]
                
        else:
            options = self.lists[0]
            del self.lists[0]
        
            for entry in options:
                opt = options.strip()
                opt2 = opt.split(',')
                
            for entry in opt2:
                string = entry.lstrip('"').rstrip('"')
                self.final_opt.append(string)
            
            if self.lists[1].startswith('VARIABLES'):
                s = self.final_opt[0].split('"')
                self.final_opt[0] = s[1]
          
        return (self.final_opt,self.axis)
    
    def parse_body(self,names=None,axis=None):
        list2 = []
        for entry in self.lists:
            s1 = entry.strip()
            s2 = s1.rstrip('\n')
            s3 = s2.split(',')
            list2.append(s3)
        
        list3 = []
        for i in range(len(list2)):
            for j in range(len(list2[i])):
                str1 = list2[i][j]
                str2 = str1.split()
                list3.append(str2)
        
# Convert values to floats

        list4 = []        
        for i in range(len(list3)):
            for j in range(len(list3[i])):
                str3 = list3[i][j]
                list4.append(float(str3)) 
                
        col_num = len(names)  
        col_two = len(names)+2
        
        if axis == 'I':
            del list4[1::col_two]
            del list4[1::col_two-1]
        elif axis == 'J':
            del list4[0::col_two]
            del list4[1::col_two-1]
        elif axis == 'K':
            del list4[0::col_two]
            del list4[0::col_two-1]
        
        return (col_num,list4)
        
        
        
        
        
        
        
        