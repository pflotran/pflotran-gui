=================
PFLOTRAN Plotter
=================

Introduction
============

The PFLOTRAN Plotter is a data visualization tool that allows PFLOTRAN users to 
easily plot and see trends of their data. The plotter was developed in Python 3.7 
and can only be used with Python 3.

Users can load their data files into the PFLOTRAN plotter by clicking the file browse
button. The plotter then loads the data and populates the first two columns of the provided 
data. The user can then click the *Add Variable* button to add a new dataset to either plot multiple
plots with one dataset each or plot all datasets on one plot. Once the data is plotted the users can
use the option menus to customize the plots features. The plot title, x axis label, and y axis label
can be edited by clicking on them and changing the input in the entry box. If multiple datasets are
plotted on one plot the color for each line can be edited by clicking on the line.

Within the files provided in the download of the PFLOTRAN Plotter, there is a file with extention .pft
that can be used to provide feedback.


Installation
============

Ensure that Python 3 is installed on your computer with the following packages:

::

 python3-matplotlib
 python3-tk

Clone this repository

::

 git clone https://bitbucket.org/pflotran/pflotran-gui

Launching PFLOTRAN Plotter
==========================

:: 

 python3 pflotran_plotter.py


How to Plot
===========

#. Click *Browse* button, navigate to PFLOTRAN .pft output file, and open it (an example file *biodegradation_hill-obs-0.pft* is provided in the pflotran-gui directory).
#. The first two columns are plotted by default. Change the variables plotted using the drop down list.
#. Added additional variables with the *Add variable* button.
#. Click on the *Plot* button to plot on separate canvases or *Plot with Multiple Datasets* to plot all variables on a single canvas.
#. Play with plot options.
