#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Created on Tue May  4 15:48:35 2021

@author: ande583
"""

import  sys
if sys.version_info.major < 3:
    sys.exit('\n  Error: {} must be run with Python 3.\n'.format(__file__))

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
try:
    # NavigationToolbar2TkAgg was deprecated in matplotlib 2.2 and is no
    # longer present in v3.0 - so a version test would also be possible.
    from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg as NavigationToolbar2Tk
except ImportError:
    from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk
except:
    raise

import tkinter as tk
from tkinter import ttk
from gui_openfile_function import open_file
from tkinter import filedialog
from tkinter.filedialog import asksaveasfile
import numpy as np
from list_sort_class import Vars
from clear_class import Clear
from parser_class import Parser
from custom_notebook import CustomNotebook
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.text import Text
from tkinter.colorchooser import askcolor
from itertools import cycle, islice
import tkinter.font as TkFont

default_font = 'DejaVu Sans'
                
class XWindow:
    def __init__(self,title,names=None):
        self.names = names
        self.title = title
        self.root = tk.Tk()
        self.root.title(title)
        self.root.geometry('1100x900')
        self.notebook = CustomNotebook(width=1100,height=900)
        self.notebook.pack(side="top", fill="both", expand=True)
        self.f = MainFrame(self.root,self.notebook)
        self.f.pack_frame()
        self.notebook.add(self.f.frame,text='Main')
        self.m = FileMenu(self.root,'File',['New File','Save as','Exit'],[self.new_file, self.save_as, self.closewindow])
        self.f.frame.grid_columnconfigure(4, minsize=100)
        
    def run(self):
        self.root.mainloop()
        
    def save_as(self):
        asksaveasfile(filetypes =(('All Files','*.*'),('tec files','*.tec'),('dat files','*.dat'),('mat files','*.mat'),('text files','*.txt')))
        
    def new_file(self):
        MainFrame.browse(self.f)
        
    def closewindow(self):
        self.root.destroy()
        
class MainLabel:
    def __init__(self,frame,text):
        self.text = text
        self.tbox1 = ttk.Label(frame,text=self.text)
        self.labels_list = []
        self.labels_list.append(self.tbox1)
        
    def position_label(self,row,col):
        self.tbox1.grid(row=row,column=col,padx=10)
        
    def change_label(self,text):
        self.text = text
        
class MainButton:
    def __init__(self,frame,text,command=None):
        self.button1 = ttk.Button(frame,text=text,command=command)
        self.button_list = []
        self.button_list.append(self.button1)
        
    def pack_button(self):
        self.button1.pack()
        
    def position_button(self,row,col):
        self.button1.grid(row=row,column=col)
        
    def place_button(self,rely,relx,x,y,anchor):
        self.button1.place(rely=rely,relx=relx,x=x,y=y,anchor=anchor)
          
        
class Variables:
    def __init__(self,name,i,var_type):
        self.name = name
        self.v = var_type
        self.v.set(self.name[i])
        self.v1 = self.v.get()
        
    def set_variable(self,names):
        self.v.set(names[0])
        
class FileMenu:
    def __init__(self,window,tab_name,func_names,funcs):
        menubar = tk.Menu(window)
        tab = tk.Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = tab_name, menu = tab)
        for i in range(len(func_names)):
            tab.add_command(label = func_names[i], command = funcs[i])
        window.config(menu = menubar)
        
class MainFrame:
    def __init__(self,window,notebook):
        self.notebook = notebook
        self.window = window
        self.frame = ttk.Frame(self.window)
        self.e = ttk.Entry(self.frame,width=50)
        self.e.grid(row=0,column=1,columnspan=3,sticky='w',padx=10)
        self.e1 = ttk.Entry(self.frame)
        self.b = MainButton(self.frame,'Browse',self.browse)
        self.b.position_button(0,4)
        self.c = MainButton(self.frame,'Close',lambda:XWindow.closewindow(x))
        self.c.place_button(1, 1, 0, 0, tk.SE)
        self.lab = MainLabel(self.frame,'File Path:')
        self.lab.position_label(0,0)
        self.widgets_list = []
        self.drop_down_labels = ['x-axis:','Dataset 1:','Dataset 2:','Dataset 3:']
        self.reps = 1
        
    def pack_frame(self):    
        self.frame.pack(fill='both',expand=1)
    
    def browse(self):
        self.reps = 1
     #   Initialize the clear class to clear all widgets and labels from the frame when another file is opened
        c = Clear(self.frame,self.widgets_list,self.drop_down_labels,1,0,-1)
        c.clear_all()
        
        self.window.filename = \
          filedialog.askopenfilename(title='Select A File',
                                     filetypes=(('All Files','*.*'),
                                                ('dat files','*.dat'),
                                                ('mat files','*.mat'),
                                                ('observation files','*.pft'),
                                                ('snapshot files','*.tec'),
                                                ('text files','*.txt')))

        self.e.insert(0,self.window.filename)
        self.e.delete(0,tk.END)

    # filename is saved in a variable   
        full_filename = self.window.filename

    # Open the file    
        f = open_file(full_filename,'r')    
        self.list = f.readlines()
        f.close()
       
        self.headings = []
    # Parse the first line of the data file to find the names of the columns of data
        parser = Parser(self.list)
        self.names, MainFrame.axis  = parser.parse_first()
        
        for i in range(len(self.names)):
            self.headings.append(self.names[i])
        
        MainFrame.headings = self.headings
        self.entry_select(self.reps)
        
    def entry_select(self,val,event=None):
        self.reps = int(val)
        
        self.var_names = []
        self.var_objects = []
        self.var_copy = []
        
        for i in range(2):
            n = Variables(self.names,i,tk.StringVar())
            self.var_names.append(n.v1)
            self.var_objects.append(n.v)
            self.var_copy.append(n.v)

        for i in range(2):
            CreateDropDowns(self.frame,self.var_objects,self.names[i],self.names,i,self.drop_down_labels,self.widgets_list)
        
        z = Clear(self.frame,self.widgets_list,self.drop_down_labels,1,0,self.reps)
        z.clear()
        z.clear_labels()
        
        p = MainButton(self.frame,'Plot',self.plot)            
        p.position_button(3,4)
        add_yaxis = MainButton(self.frame,'Add variable',self.add_yaxis)
        add_yaxis.position_button(4,4)
        del_yaxis = ttk.Button(self.frame,text='Delete variable',command=self.del_yaxis)
        del_yaxis.grid(row=5,column=4)
        multiple_lines = ttk.Button(self.frame,text='Plot with Multiple Datasets',command=lambda:self.multiple_lines(1))
        multiple_lines.grid(row=3,column=5)
        
    def del_yaxis(self):
        self.reps -= 1
        
        if self.reps >= 3:
            del self.drop_down_labels[-1]
        
        if len(self.var_objects) >= 1:
            del self.var_objects[-1]
        
        if self.reps >= len(self.names)-1:
            del self.headings[-1]
        
        if self.reps >= 1:
            z = Clear(self.frame,self.widgets_list,self.drop_down_labels,1,0,self.reps)
            z.clear()
            z.clear_labels()
        
    def add_yaxis(self):
        self.reps += 1
        if self.reps >= len(self.drop_down_labels):
                 st = str(self.reps)
                 l = 'Dataset '+st+':'
                 self.drop_down_labels.append(l)
        
        if self.reps >= len(self.names):
            self.var_names = self.headings[-1]
            self.headings.append(self.headings[-1])
        else:
            self.var_names = self.headings[self.reps]
        
        n1 = Variables(self.headings,self.reps,tk.StringVar())
        self.var_objects.append(n1.v)
        self.var_copy.append(n1.v)
            
        CreateDropDowns(self.frame,self.var_objects,self.var_names,self.names,self.reps,self.drop_down_labels,self.widgets_list)
        
    def multiple_lines(self,event):
        self.plot2 = PlotPage('Plot',1,self.list,self.var_objects,self.var_copy,self.names,self.reps,event)
    
    def plot(self):
        self.plot1 = PlotPage('Plot',self.reps,self.list,self.var_objects,self.var_copy,self.names,self.reps)
        
class PlotPage:
    def __init__(self,text,reps,list1,var_objects,var_copy,names,num_of_axis,event=None):
        self.tabframe = ttk.Frame(x.notebook)
        self.tabframe.pack(side=tk.TOP,fill='both',expand=1)
        x.notebook.add(self.tabframe,text=text)
        scroll = XScrollbar(self.tabframe)
        self.p_frames = []
        PlotPage.drop_list = []
        PlotPage.plots = []
        
        for i in range(reps):
            self.myframe = PlotFrame(scroll.scrollbarframe,i,list1,var_objects,var_copy,names,num_of_axis,event)  
            self.p_frames.append(self.myframe)
            
        x.notebook.select(self.tabframe)
            
    def clear_frames(self):             
        for frame in self.p_frames:
            for widget in frame.winfo_children():
                widget.destroy()
            
class PlotFrame:
    def __init__(self,frame,i,list1,var_objects,var_copy,names,num_of_axis,event=None):
        self.myframe1 = ttk.Frame(frame,width=1100,height=850)
        self.myframe1.pack(side=tk.TOP,fill='both',expand=1)
        self.myframe1.pack_propagate(0)
        PlotCanvas(list1,i,self.myframe1,var_objects,var_copy,names,num_of_axis,event)
                   
class PlotCanvas:
    def __init__(self,list1,i,p_frames,var_objects,var_copy,names,num_of_axis,event=None):
        self.dats = DataFileOrganizer(p_frames,list1,var_objects,var_copy,names,num_of_axis)
        data = self.dats.a.yaxis()
        frames = p_frames
        self.graph = Plotter(frames,self.dats.x,self.dats.x_label,i,data,num_of_axis,event)
        self.graph.plotwidgets()               
                     
class DataFileOrganizer:
    def __init__(self,p_frames,list1,var_objects,var_copy,names,num_of_axis):
        
        parse = Parser(list1)
        self.col_num = parse.parse_body(names,MainFrame.axis)[0]
        list4 = parse.parse_body(names,MainFrame.axis)[1]
        self.values = np.array(list4)
        
        for i in range(len(var_objects)):
            var_objects[i] = var_copy[i].get()
        
    # Vars is instantiated to sort the data by column according to its column name
        for i in range(num_of_axis+1):
            self.a = Vars(var_objects,names,self.values,self.col_num,num_of_axis)
        
    # Arrays are created and assigned to variables that will be used in plotting 
        self.x = np.array(self.a.xaxis()[1])
        self.x_label = self.a.xaxis()[0]
            
class XScrollbar:
    def __init__(self,tabframe):
        self.tabframe = tabframe
        self.my_canvas = tk.Canvas(self.tabframe)
        my_vbar=ttk.Scrollbar(self.tabframe, orient = tk.VERTICAL,command=self.my_canvas.yview)
        my_vbar.pack(side= tk.RIGHT, fill= tk.Y)
        my_hbar=ttk.Scrollbar(self.tabframe, orient = tk.HORIZONTAL,command=self.my_canvas.xview)
        my_hbar.pack(side= tk.BOTTOM, fill= tk.X)
        self.scrollbarframe = ttk.Frame(self.my_canvas)
        self.my_canvas.create_window((0,0), window=self.scrollbarframe,anchor="nw", tags="self.tabframe")
        self.my_canvas.configure(yscrollcommand=my_vbar.set)
        self.my_canvas.configure(xscrollcommand=my_hbar.set)
        self.my_canvas.bind('<Configure>',self.on_resize)
        self.my_canvas.pack(side=tk.LEFT,fill=tk.BOTH, expand=1)
        
    def on_resize(self,event):
        width = event.width - 4
        self.my_canvas.itemconfigure('self.tabframe', width=width)
        self.my_canvas.configure(scrollregion=self.my_canvas.bbox("all"))
        

class CreateDropDowns:
    def __init__(self,frame,var_objects,var_names,vals,i,labels=None,widgets_list=None,command=None):
        d = ttk.OptionMenu(frame,var_objects[i],var_names,*vals)
        widgets = widgets_list[:] + [d]
        d.grid(row=i+3,column=1)
        d_label = MainLabel(frame,text=labels[i])
        d_label.position_label(i+3,0)
        widgets_list.append(widgets)
        d.config(width=len(max(vals,key=len)))
        
class FontPage:
    def __init__(self,window,labels,row,fontname,fontsize,fontstyle):
        fonts = TkFont.families()
        num_list = list(range(31))
        style = ['normal','oblique','italic']
        self.font1 = tk.StringVar()
        self.fontsize1 = tk.StringVar()
        self.style1 = tk.StringVar()
        self.font1.set(fontname)
        self.fontsize1.set(fontsize)
        self.style1.set(fontstyle)
        self.fonts = [self.font1,self.fontsize1,self.style1]
        frame = ttk.Frame(window)
        frame.pack()
        self.listbox = ttk.Combobox(frame,textvariable=self.font1,values=fonts)
        box_label = ttk.Label(frame,text=labels+' Font:')
        box_label.grid(row=row+2,column=0)
        self.listbox.grid(row=row+3,column=0)
        self.listbox2 = ttk.Combobox(frame,textvariable=self.fontsize1,values=num_list)
        box_label1 = ttk.Label(frame,text=labels+' Fontsize:')
        box_label1.grid(row=row+2,column=1)
        self.listbox2.grid(row=row+3,column=1)
        self.listbox3 = ttk.Combobox(frame,textvariable=self.style1,values=style)
        box_label2 = ttk.Label(frame,text=labels+' Style:')
        box_label2.grid(row=row+2,column=2)
        self.listbox3.grid(row=row+3,column=2)

class PopupWindow:
    def __init__(self,master,labels,xlabel,ylabel,title,axis,figure,fontname,fontsize,fontstyle):
        self.axis = axis
        self.figure = figure
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.title = title
        PopupWindow.fontname = fontname
        PopupWindow.fontsize = fontsize
        PopupWindow.fontstyle = fontstyle
        top=self.top=tk.Toplevel(master)
        top.geometry('2000x700')
        self.e1=ttk.Entry(top)
        self.e2=ttk.Entry(top)
        self.e3=ttk.Entry(top)
        self.e4=ttk.Entry(top)
        if self.xlabel == labels.get_text() or self.ylabel == labels.get_text() or self.title == labels.get_text():
            self.l= ttk.Label(top,text="Update Labels")
            self.l.pack()
            self.f1 = Figure(dpi = 100,tight_layout=True)
            self.canvas1 = FigureCanvasTkAgg(self.f1,self.top)
            self.ax1 = self.f1.add_subplot(111)
            self.canvas1._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
            lab1 = ttk.Label(top,text='X-label:')
            lab1.pack(side=tk.LEFT)
            self.e1.insert(0,self.xlabel)
            self.e1.pack(side=tk.LEFT)
            lab2 = ttk.Label(top,text='Y-label:')
            lab2.pack(side=tk.LEFT)
            self.e2.insert(0,self.ylabel)
            self.e2.pack(side=tk.LEFT)
            lab3 = ttk.Label(top,text='Title:')
            lab3.pack(side=tk.LEFT)
            self.e3.insert(0,self.title)
            self.e3.pack(side=tk.LEFT)
            lab4 = ttk.Label(top,text='Subscripts Example:')
            lab4.pack(side=tk.LEFT)
            lab5 = ttk.Label(top,text='Use latex to add subscripts and superscripts')
            lab5.pack(side=tk.LEFT)
            self.e4.insert(0,' $e^{\sin(\omega\phi)}$')
            self.e4.pack(side=tk.LEFT)
            self.ax1.set_title(self.e4.get())
            self.ax1.set_xlabel(self.e1.get())
            self.ax1.set_ylabel(self.e2.get())
            self.ok = ttk.Button(top,text='Apply',command=self.ok)
            self.ok.pack(side=tk.LEFT)
            self.button3 = ttk.Button(top,text='Change Font',command=lambda:self.fonts(master))
            self.button3.pack(side=tk.LEFT)
            self.canvas1.draw_idle()
        self.b=ttk.Button(top,text='Ok',command=self.cleanup)
        self.top.protocol('WM_DELETE_WINDOW',self.leave)
        self.b.place(rely=1,relx=1,x=0,y=0,anchor=tk.SE)
        
    def ok(self):
        self.ax1.set_title(self.e3.get()+self.e4.get())
        self.ax1.set_xlabel(self.e1.get())
        self.ax1.set_ylabel(self.e2.get())
        self.canvas1.draw_idle()
    
    def leave(self):
        self.cleanup()
        self.top.destroy()
        
    def fonts(self,master):
        labels = ['All Labels']
        top2=self.top2=tk.Toplevel(master)
        self.fnt = FontPage(self.top2,labels[0],0,self.fontname,self.fontsize,self.fontstyle)
        font_button = ttk.Button(top2,text='Apply',command=self.update_font)
        font_button.pack()
        
    def update_font(self):
        self.fnt.listbox.set(self.fnt.listbox.get())
        self.fnt.listbox2.set(self.fnt.listbox2.get())
        self.fnt.listbox3.set(self.fnt.listbox3.get())
        self.ax1.set_title(self.title,picker=True,fontname=self.fnt.listbox.get(),fontsize=self.fnt.listbox2.get(),fontweight='normal',fontstyle=self.fnt.listbox3.get())
        self.ax1.set_xlabel(self.xlabel,picker=True,fontname=self.fnt.listbox.get(),fontsize=self.fnt.listbox2.get(),fontweight='normal',fontstyle=self.fnt.listbox3.get())
        self.ax1.set_ylabel(self.ylabel,picker=True,fontname=self.fnt.listbox.get(),fontsize=self.fnt.listbox2.get(),fontweight='normal',fontstyle=self.fnt.listbox3.get())
        self.canvas1.draw_idle()
    
    def cleanup(self):
        PopupWindow.xlab = self.e1.get()
        PopupWindow.ylab = self.e2.get()
        PopupWindow.titles = self.e3.get()
        PopupWindow.fontname = self.fnt.listbox.get()
        PopupWindow.fontsize = self.fnt.listbox2.get()
        PopupWindow.fontstyle = self.fnt.listbox3.get()
        self.top.destroy()
        
class Line:
    def __init__(self,x_data,y_data,graphtype=None,linewidth=None,linestyle=None,color=None,
                 marker=None,markersize=None,label=None,picker=None,attributes=None):
        self.x_data = x_data
        self.y_data = y_data
        self.graphtype = graphtype
        self.linewidth = linewidth
        self.linestyle = linestyle
        self.color = color
        self.marker = marker
        self.markersize = markersize
        self.label = label
        self.picker = picker
        self.attributes = attributes
        
        self.line = {'x':self.x_data,'y':self.y_data,'graphtype':self.graphtype[0],
                     'linestyle':self.linestyle[0],'color':self.color,'linewidth':self.linewidth[0],
                     'marker':self.marker[0],'markersize':self.markersize[0],'label':self.label,
                     'picker':self.picker}
                            
class Plotter:
    DEFAULT_LINE_TYPE = 'Line graph'
    DEFAULT_COLOR = 'Blue'
    DEFAULT_MARKER = 'None'
    DEFAULT_LINESTYLE = 'solid'
    DEFAULT_LINEWIDTH = 1
    DEFAULT_MARKERSIZE = 10
    def __init__(self,master,xval=None,xlabels=None,i=None,data=None,num_of_axis=None,event=None):
        self.master = master
        self.default_font = default_font
        self.i = i
        self.xval = xval
        self.data = data
        self.num_of_axis = num_of_axis
        self.xlabels = xlabels
        self.event = event
        self.fontsize = 12
        self.fontstyle = 'normal'
        self.ylist = []
        self.y1list = []
        self.ylabels = []
        self.y1labels = []
        self.title_list = []
        self.title1_list = []
        self.ylim_lower_lim = []
        self.ylim_upper_lim = []
        self.lines_list = []
        
        if self.event == 1:
            for i in range(self.num_of_axis):
                y = np.array(self.data[i][2])
                self.y1list.append(y)
                y_labels = np.array(self.data[i][1])
                self.y1labels.append(y_labels)
                titles = np.array(self.data[i][0])
                self.title1_list.append(titles)
                self.ylabel = 'Y-axis'
                self.title = 'Plot'
                self.ylim_lower = min(self.y1list[i])
                self.ylim_upper = max(self.y1list[i])
                self.ylim_lower_lim.append(self.ylim_lower)
                self.ylim_upper_lim.append(self.ylim_upper)
                self.ylim_lower = min(self.ylim_lower_lim)
                self.ylim_upper = max(self.ylim_upper_lim)      
        else:
            y = np.array(self.data[self.i][2])
            self.ylist.append(y)
            y_labels = np.array(self.data[self.i][1])
            self.ylabels.append(y_labels)
            titles = np.array(self.data[self.i][0])
            self.title_list.append(titles)
            self.y = self.ylist[0]
            self.title = self.title_list[0]
            self.title1 = self.title
            self.ylabel = self.ylabels[0]
            self.ylim_lower = min(self.y)
            self.ylim_upper = max(self.y)   
        self.x = self.xval
        self.xlabel = self.xlabels
        self.xlim_lower = 0
        self.xlim_upper = max(self.x)+1
        self.graphtype = self.DEFAULT_LINE_TYPE
        self.linestyle = self.DEFAULT_LINESTYLE
        self.marker = self.DEFAULT_MARKER
        self.markersize = self.DEFAULT_MARKERSIZE
        self.color = self.DEFAULT_COLOR
        self.linewidth = self.DEFAULT_LINEWIDTH
        self.f = Figure(dpi = 100,tight_layout=True)
        self.canvas = FigureCanvasTkAgg(self.f,self.master)
        self.ax = self.f.add_subplot(111)
        self.canvas.mpl_connect('pick_event',self.onpick)
        self.graphtypes = ['Line graph','Scatter plot']
        self.colors = ['Blue','Green','Red','Cyan','Magenta','Yellow','Black','White','Choose a different color']
        colors_cycled = cycle(['Blue','Green','Red','Cyan','Magenta','Yellow','Black'])
        sliced = islice(colors_cycled,None,self.num_of_axis)
        self.result = list(sliced)
        self.markers = ['None','.',',','o','<','>','v','^','1','2','3','4','8','s','p','P','*','x','X','h','H','+','d','D']
        self.markersizes = ['10','50','75','100','150','200','250','300','350','400','450','500','Change Markersize']
        self.linestyles = ['solid','dotted','dashed','dashdot']
        self.linewidths = ['1','2','3','4','5','6','7','8','9','10','Change Linewidth']
        self.custom = [self.graphtypes,self.linestyles,self.linewidths,self.markers,self.markersizes]
        self.first_entry = ['      Graph Type','Line Style','Line Width','Marker Type','Marker Size']
        self.commands = [self.set_type,self.set_linestyle,self.set_linewidth,self.set_marker,self.set_markersize]
        self.locations = ['best','upper left','upper right','lower left','lower right','upper center','lower center','center left','center right','center']
        self.yscale = ['linear','log']
        self.stats_button = ttk.Button(self.master,text='Statistical Analysis',command=self.compute_stats)
        self.legend_button = ttk.Button(self.master,text='Legend On',command=self.legend)
        self.var = tk.StringVar()
        self.var1 = tk.StringVar()
        self.legend_location = ttk.OptionMenu(self.master,self.var,self.locations[0],*self.locations,command=lambda e:self.legend(1))
        self.grid_button = ttk.Button(self.master,text='Grid On',command=self.grid)
        self.log = ttk.OptionMenu(self.master,self.var1,self.yscale[0],*self.yscale,command= lambda e:self.log_scale(self.var1.get()))
        self.titles_button = ttk.Button(self.master,text='Titles',command=self.titles_off)
        self.line_list = []
        
        if self.event == 1:
            Plotter.plotoptions_list = []
            self.graphtype1 = tk.StringVar()
            self.linestyle1 = tk.StringVar()
            self.marker1 = tk.StringVar()
            self.linewidth1 = tk.StringVar()
            self.markersize1 = tk.StringVar()
            self.color1 = tk.StringVar()
            Plotter.plotoptions_list.append([self.graphtype1,self.linestyle1,self.linewidth1,self.marker1,self.markersize1])
            names = MainFrame.headings[1:]
            
            for i in range(self.num_of_axis):
                line = Line(self.x,self.y1list[i],self.graphtypes,self.linewidths,self.linestyles,self.result[i],self.markers,self.markersizes,names[i],True,Plotter.plotoptions_list)
                self.line_list.append(line.line)
                
        self.change_axis_range = ttk.Button(self.master,text='Change Axis Range',command=self.change_range)
            
                
    def titles_off(self):
        if self.titles_button['text'] == 'Titles':
            self.titles_button.configure(text='No Titles')
            self.ax.set_title('')
            self.ax.set_xlabel('')
            self.ax.set_ylabel('')
            self.canvas.draw_idle()
        else:
            self.titles_button.configure(text='Titles')
            if self.title == '':
                self.title = 'Title'
            if self.xlabel == '':
                self.xlabel = 'X-axis'
            if self.ylabel == '':
                self.ylabel = 'Y-axis'
                
            self.ax.set_title(self.title,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            self.ax.set_xlabel(self.xlabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            self.ax.set_ylabel(self.ylabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            self.canvas.draw_idle()
                
    def log_scale(self,scale=None):
        if scale == 'log':
            self.ax.set_yscale('log')
            self.canvas.draw_idle()
        else:
            self.plotgraph()
        
    def grid(self):
        if self.grid_button['text'] == 'Grid On':
            self.grid_button.configure(text='Grid Off')
            self.ax.grid(False)
            self.canvas.draw_idle()
        else:
            self.grid_button.configure(text='Grid On')
            self.ax.grid()
            self.canvas.draw_idle()
        
    def legend(self,num=None):
        if num == 1:
            if self.legend_button['text'] == 'Legend Off':
                pass
            else:
                leg = self.ax.legend(loc=self.var.get())
                self.pick_legend(leg,self.line_list)
                self.canvas.draw_idle()
        else:
            if self.legend_button['text'] == 'Legend On':
                self.legend_button.configure(text='Legend Off')
                self.ax.get_legend().remove()
                self.canvas.draw_idle()
            else:
                self.legend_button.configure(text='Legend On')
                leg = self.ax.legend(loc=self.var.get())
                self.pick_legend(leg,self.line_list)
                self.canvas.draw_idle()
    
    def plotwidgets(self):
        toolbar = NavigationToolbar2Tk(self.canvas,self.master)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
        if self.event == 1:
            self.stats_button.pack(side=tk.LEFT)
            self.legend_button.pack(side=tk.LEFT)
            self.grid_button.pack(side=tk.LEFT)
            self.titles_button.pack(side=tk.LEFT)
            self.change_axis_range.pack(side=tk.LEFT)
        else:
            self.change_axis_range.pack(side=tk.BOTTOM)
            self.stats_button.pack(side=tk.BOTTOM)
            self.legend_button.pack(side=tk.BOTTOM)
            self.grid_button.pack(side=tk.BOTTOM)
            self.titles_button.pack(side=tk.BOTTOM)
        self.legend_label = ttk.Label(self.master,text='   Legend Location:')
        self.legend_label.pack(side=tk.LEFT)
        self.legend_location.pack(side=tk.LEFT)
        self.yscale_label = ttk.Label(self.master,text='Y-scale:')
        self.yscale_label.pack(side=tk.LEFT)
        self.log.pack(side=tk.LEFT)
        
        if self.event == 1:
            pass
        
        else:
            self.plotoptions_list = []
            self.graphtype1 = tk.StringVar()
            self.color1 = tk.StringVar()
            self.linestyle1 = tk.StringVar()
            self.marker1 = tk.StringVar()
            self.linewidth1 = tk.StringVar()
            self.markersize1 = tk.StringVar()
            self.plotoptions_list.append([self.graphtype1,self.linestyle1,self.linewidth1,self.marker1,self.markersize1])
            
            for j in range(len(self.plotoptions_list[0])):
                rdb = ttk.OptionMenu(self.master,self.plotoptions_list[0][j],self.custom[j][0],*self.custom[j],command=self.commands[j])
                lab = ttk.Label(self.master,text=self.first_entry[j]+' :')
                lab.pack(side=tk.LEFT)
                rdb.pack(side=tk.LEFT)
            
            self.color = self.result[self.i]
            self.color_menu = ttk.OptionMenu(self.master,self.color1,self.result[self.i],*self.colors,command=self.choose_color)
            color_label = ttk.Label(self.master,text='Color:')
            color_label.pack(side=tk.LEFT)
            self.color_menu.pack(side=tk.LEFT)
        self.plotgraph()
        
    def onpick(self,event):
        if isinstance(event.artist, Line2D):
            Plotter.line = event.artist
            Plotter.line_label = Plotter.line.get_label()
            self.top = tk.Toplevel(self.master)
            if self.event == 1:
                for j in range(len(Plotter.plotoptions_list[0])):
                    rdb = ttk.OptionMenu(self.top,Plotter.plotoptions_list[0][j],self.custom[j][0],
                                         *self.custom[j],command=self.commands[j])
                    lab = ttk.Label(self.top,text=self.first_entry[j]+' :')
                    lab.pack(side=tk.LEFT)
                    rdb.pack(side=tk.LEFT)
                
                self.color = self.result[self.i]
                self.color_menu = ttk.Button(self.top,text='Change Color',command= self.line_color)
                color_label = ttk.Label(self.top,text='Color:')
                color_label.pack(side=tk.LEFT)
                self.color_menu.pack(side=tk.LEFT)

            label = ttk.Label(self.top,text='Change Label: ')
            entry = ttk.Entry(self.top)
            label.pack(side=tk.LEFT)
            entry.insert(0,Plotter.line_label)
            entry.pack(side=tk.LEFT)
            button = ttk.Button(self.top,text='OK',command=lambda:self.change_label(entry.get()))
            button.pack(side=tk.LEFT)
                
        if isinstance(event.artist, Text):
            label = event.artist
            self.update_labels(label)
            
    def change_label(self,label):
        index = 0
        if self.event == 1:
            for line in self.line_list:
                if line['label'] == Plotter.line_label:
                    self.leg.texts[index].set_text(label)
                index += 1
            self.canvas.draw_idle()
            self.top.destroy()
        else:
            for line in self.lines_list:
                self.leg.texts[0].set_text(label)
                self.canvas.draw_idle()
            self.top.destroy()
                
    def line_color(self):
        self.color = askcolor(color=self.color)[1]
        for i in range(len(self.line_list)):
            if self.line_list[i]['label'] == Plotter.line_label:
                self.line_list[i]['color'] = self.color
        self.plotgraph(2)
               
    def update_labels(self,event):
        label = event
        self.pop = PopupWindow(self.master,label,self.xlabel,self.ylabel,self.title,self.ax,self.f,self.default_font,self.fontsize,self.fontstyle)
        self.master.wait_window(self.pop.top)
        self.entryvalue()
        self.plotgraph()
        
    def entryvalue(self):
        self.xlabel = PopupWindow.xlab
        self.ylabel = PopupWindow.ylab
        self.title = PopupWindow.titles
        self.default_font = PopupWindow.fontname
        self.fontsize = PopupWindow.fontsize
        self.fontstyle = PopupWindow.fontstyle
        if self.title == '':
            self.title = ''
        if self.xlabel == '':
            self.xlabel = ''
        if self.ylabel == '':
            self.ylabel = ''
            
    def pick_legend(self,legend_lines,line_list):
        if self.event == 1:
            leg = legend_lines
            lined = dict()
            for legline, origline in zip(leg.get_lines(), line_list[0]):
                legline.set_picker(7)  # Enable picking on the legend line.
                lined[legline] = origline
        else:
            leg = legend_lines
            lined = dict()
            for legline, origline in zip(leg.get_lines(), line_list):
                legline.set_picker(7)  # Enable picking on the legend line.
                lined[legline] = origline
            
    def make_lines(self,line_list):
        lines = []
        for i in range(len(line_list)):
            if line_list[i].get('graphtype') == 'Line graph':
                line = self.ax.plot(line_list[i].get('x'),line_list[i].get('y'),ls=line_list[i].get('linestyle'),
                             c=line_list[i].get('color'),lw=line_list[i].get('linewidth'),
                             marker=line_list[i].get('marker'),markersize=line_list[i].get('markersize'),
                             label=line_list[i].get('label'),picker=True)
                lines.append(line)
                
            if line_list[i].get('graphtype') == 'Scatter plot':
                line = self.ax.scatter(x=line_list[i].get('x'),y=line_list[i].get('y'),s=float(line_list[i].get('markersize')),
                                linewidths=int(line_list[i].get('linewidth')),c=line_list[i].get('color'),
                                marker=line_list[i].get('marker'),label=line_list[i].get('label'))
                lines.append(line)
                
        self.ax.set_ylim(self.ylim_lower,self.ylim_upper)
        self.ax.set_xlim(self.xlim_lower,self.xlim_upper)
        self.ax.grid()
        self.leg = self.ax.legend()
        self.pick_legend(self.leg,line_list)
        self.canvas.draw_idle()
            
    def plotgraph(self,event=None):
        self.ax.clear()
        if self.event == 1:
            if event == 2:
                self.ax.set_title(self.title,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
                self.make_lines(self.line_list)
            else:
                self.ax.set_title(self.title,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
                self.ax.set_xlabel(self.xlabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
                self.ax.set_ylabel(self.ylabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
                for i in range(self.num_of_axis):
                    if self.graphtype == 'Line graph':
                        line = self.ax.plot(self.x,self.y1list[i],ls=self.linestyle,c=self.result[i],marker=self.marker,markersize=self.markersize,label=self.title1_list[i],picker=True)
                        self.ax.set_ylim(self.ylim_lower,self.ylim_upper)
                        self.ax.set_xlim(self.xlim_lower,self.xlim_upper)
                        self.ax.grid(True)
                        self.leg = self.ax.legend()
                        self.pick_legend(self.leg,self.line_list)
                        self.canvas.draw_idle()
                    if self.graphtype == 'Scatter plot':
                        self.sc = self.ax.scatter(self.x,self.y1list[i],marker=self.marker,s=self.markersize,c=self.result[i],label=self.title1_list[i],picker=True)
                        self.ax.set_ylim(self.ylim_lower,self.ylim_upper)
                        self.ax.set_xlim(self.xlim_lower,self.xlim_upper)
                        self.ax.grid(True)
                        self.leg = self.ax.legend()
                        self.pick_legend(self.leg,self.line_list)
                        self.canvas.draw_idle()
        
        else:
            self.ax.set_xlabel(self.xlabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            self.ax.set_ylabel(self.ylabel,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            self.ax.set_title(self.title,picker=True,fontname=self.default_font,fontsize=self.fontsize,fontweight='normal',fontstyle=self.fontstyle)
            if self.graphtype == 'Line graph':
                line = self.ax.plot(self.x,self.y,ls=self.linestyle,c=self.color,lw=self.linewidth,marker=self.marker,markersize=self.markersize,label=self.title)
                self.lines_list.append(line)
                self.ax.set_ylim(self.ylim_lower,self.ylim_upper)
                self.ax.set_xlim(self.xlim_lower,self.xlim_upper)
                self.ax.grid()
                self.leg = self.ax.legend()
                self.pick_legend(self.leg,line)
                self.canvas.draw_idle()
            if self.graphtype == 'Scatter plot':
                scatter = self.ax.scatter(self.x,self.y,marker=self.marker,s=self.markersize,c=self.color,label=self.title)
                self.lines_list.append(scatter)
                self.ax.set_ylim(self.ylim_lower,self.ylim_upper)
                self.ax.set_xlim(self.xlim_lower,self.xlim_upper)
                self.ax.grid()
                self.leg = self.ax.legend()
                self.pick_legend(self.leg,scatter)
                self.canvas.draw_idle()
                
    def change_range(self):
        ymin,ymax = self.ax.get_ylim()
        xmin,xmax = self.ax.get_xlim()
        top = self.top = tk.Toplevel(self.master)
        self.entry = ttk.Entry(top)
        label = ttk.Label(top,text='Change Axis Limits')
        label.grid(row=0,column=0)
        label1 = ttk.Label(top,text='Y-axis Upper Limit:')
        label1.grid(row=2,column=0)
        self.entry.grid(row=2,column=1)
        label2 = ttk.Label(top,text='Y-axis Lower Limit:')
        label2.grid(row=1,column=0)
        self.entry1 = ttk.Entry(top)
        self.entry1.grid(row=1,column=1)
        label3 = ttk.Label(top,text='X-axis Upper Limit:')
        label3.grid(row=4,column=0)
        self.entry2 = ttk.Entry(top)
        self.entry2.grid(row=4,column=1)
        label4 = ttk.Label(top,text='X-axis Lower Limit:')
        label4.grid(row=3,column=0)
        self.entry3 = ttk.Entry(top)
        self.entry3.grid(row=3,column=1)
        self.entry.insert(0,ymax)
        self.entry1.insert(0,ymin)
        self.entry2.insert(0,xmax)
        self.entry3.insert(0,xmin)
        button = ttk.Button(top,text='Ok',command=self.set_axis)
        button.grid(row=3,column=2)
        
    def set_axis(self):
        self.ylim_lower = float(self.entry1.get())
        self.ylim_upper = float(self.entry.get())
        self.xlim_lower = float(self.entry3.get())
        self.xlim_upper = float(self.entry2.get())
        self.plotgraph()
        self.top.destroy()
    
    def compute_stats(self):
        stats_list = []
        mean_list = []
        median_list = []
        mins_list = []
        maxs_list = []
        range_list = []
        self.top = tk.Toplevel(self.master)
        scroll = XScrollbar(self.top)
        self.l= ttk.Label(scroll.scrollbarframe,text="Statistical Analysis")
        self.l.pack()
        if self.event == 1:
            for i in range(self.num_of_axis):
                mean = np.mean(self.y1list[i])
                mean_list.append(mean)
                median = np.median(self.y1list[i])
                median_list.append(median)
                minimum = min(self.y1list[i])
                mins_list.append(minimum)
                maximum = max(self.y1list[i])
                maxs_list.append(maximum)
                ranges = maximum-minimum
                range_list.append(ranges)
                stats_list.append(['{}'.format(self.title1_list[i]),'Mean: {}\n'.format(mean_list[i]),'Median: {}\n'.format(median_list[i]),'Minimum: {}\n'.format(mins_list[i]),'Maximum: {} \n'.format(maxs_list[i]),'Range: {} \n'.format(range_list[i])])
                for j in range(len(stats_list[0])):
                    lab = ttk.Label(scroll.scrollbarframe, text=stats_list[i][j])
                    lab.pack()
    
        else:
            mean = np.mean(self.y)
            medians = np.median(self.y)
            minimum = min(self.y)
            maximum = max(self.y)
            ranges = maximum-minimum
            stats_list.append(['Mean: {}\n'.format(mean),'Median: {}\n'.format(medians),'Minimum: {}\n'.format(minimum),'Maximum: {} \n'.format(maximum),'Range: {} \n'.format(ranges)])
            for i in range(len(stats_list[0])):
                lab = ttk.Label(scroll.scrollbarframe, text=stats_list[0][i])
                lab.pack()
        self.b=ttk.Button(scroll.scrollbarframe,text='Ok',command=self.close)
        self.b.pack()
    
    def close(self):
        self.top.destroy()
    
    def set_type(self,*args):
        for entry in self.graphtypes:
            if self.graphtype1.get() == entry:
                self.graphtype = entry
        if self.event == 1:
            for i in range(len(self.line_list)):
                if self.line_list[i]['label'] == Plotter.line_label:
                    self.line_list[i]['graphtype'] = self.graphtype
            self.plotgraph(2)
        else:
            self.plotgraph()
    
    def set_linewidth(self,*args):
        for entry in self.linewidths:
            if self.linewidth1.get() == entry:
                if self.linewidth1.get() == 'Change Linewidth':
                    top = self.top = tk.Toplevel(self.master)
                    self.entrybox = ttk.Entry(top)
                    self.l= ttk.Label(top,text="Change Linewidth")
                    self.l.pack()
                    lab = ttk.Label(top,text='Linewidth:')
                    lab.pack(side=tk.LEFT)
                    self.entrybox.pack(side=tk.LEFT)
                    self.b=ttk.Button(top,text='Ok',command=self.getlinewidth)
                    self.b.pack()
                else:
                    entry = int(entry)
                    self.linewidth = entry
                    if self.event == 1 or self.event == None:
                        for i in range(len(self.line_list)):
                            if self.line_list[i]['label'] == Plotter.line_label:
                                self.line_list[i]['linewidth'] = self.linewidth1.get()
        self.plotgraph(2)
            
    def getlinewidth(self):
        if self.entrybox.get().find('.') != -1:
            self.linewidth = float(self.entrybox.get())
        else:
            self.linewidth = int(self.entrybox.get())
        self.linewidth1.set(self.linewidth)
        
        for i in range(len(self.ax.get_lines())):
            line = self.ax.get_lines()[i]
            if Plotter.line == line:
                if line.get_label() == Plotter.line_label:
                    Plotter.line.set_linewidth(self.linewidth)
                    
        for i in range(len(self.line_list)):
            if self.line_list[i]['label'] == Plotter.line_label:
                self.line_list[i]['linewidth'] = self.linewidth1.get()
                
        self.plotgraph(2)
        self.top.destroy()
        
    def choose_color(self,*args):
        for entry in self.colors:   
            if self.color1.get() == entry and self.color1.get() != 'Choose a different color':
                self.color = entry
        if self.color1.get() == 'Choose a different color':
            self.color = askcolor(color=self.color)[1]
            self.color1.set(self.color)
        self.plotgraph()
        
    def set_marker(self,*args):
        for entry in self.markers:
            if self.marker1.get() == entry:
                self.marker = entry
        if self.event == 1:
            for i in range(len(self.line_list)):
                if self.line_list[i]['label'] == Plotter.line_label:
                    self.line_list[i]['marker'] = self.marker
            self.plotgraph(2)
        else:
            self.plotgraph()
                
    def set_markersize(self,*args):
        for entry in self.markersizes:
            if self.markersize1.get() == entry:
                if self.markersize1.get() == 'Change Markersize':
                    top = self.top = tk.Toplevel(self.master)
                    self.entrybox = ttk.Entry(top)
                    self.l= ttk.Label(top,text="Change Markersize")
                    self.l.pack()
                    lab = ttk.Label(top,text='Markersize:')
                    lab.pack(side=tk.LEFT)
                    self.entrybox.pack(side=tk.LEFT)
                    self.b=ttk.Button(top,text='Ok',command=self.getmarkersize)
                    self.b.pack()
                else:
                    entry = int(entry)
                    self.markersize = entry
                    if self.event == 1 or self.event == None:
                        for i in range(len(self.line_list)):
                            if self.line_list[i]['label'] == Plotter.line_label:
                                self.line_list[i]['markersize'] = self.markersize1.get()
        self.plotgraph(2)
                    
                  
    def getmarkersize(self):
        if self.entrybox.get().find('.') != -1:
            self.markersize = float(self.entrybox.get())
        else:
            self.markersize = int(self.entrybox.get())
        self.markersize1.set(self.markersize)
        
        for i in range(len(self.line_list)):
            if self.line_list[i]['label'] == Plotter.line_label:
                self.line_list[i]['markersize'] = self.markersize1.get()
                
        self.plotgraph(2)
        self.top.destroy()
    
    def set_linestyle(self,*args):
        for entry in self.linestyles:
            if self.linestyle1.get() == entry:
                self.linestyle = entry
        if self.event == 1:
            for i in range(len(self.line_list)):
                if self.line_list[i]['label'] == Plotter.line_label:
                    self.line_list[i]['linestyle'] = self.linestyle1.get()
            self.plotgraph(2)
        else:
            self.plotgraph()
            
if __name__=="__main__":
        
    x = XWindow('PFLOTRAN Data Viewer')
    x.run()
    
    

    
